function AugmentFont(elementID) {
    var id = document.getElementById(elementID);
    var size = parseFloat(id.style.fontSize);
    id.style.fontSize = (size + .1) + "em";
}

function ReduceFont(elementID) {
    var id = document.getElementById(elementID);
    var size = parseFloat(id.style.fontSize);
    id.style.fontSize = (size - .1) + "em";
}
